package com.dwring.webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread {

	private Socket socket;
	private InputStream inputStream;
	private PrintStream out;
	private final static String WEB_ROOT = "C:\\Users\\zhanghaichang\\workspace\\webserver\\htdocs";

	public Processor(Socket socket) {
		this.socket = socket;
		try {
			inputStream = socket.getInputStream();
			out = new PrintStream(socket.getOutputStream());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run() {
		String fileName = parse(inputStream);
		sendFile(fileName);
	}

	public String parse(InputStream in) {
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(in));
		String filename = null;
		try {
			String httpMessage = bufferedReader.readLine();
			String[] content = httpMessage.split(" ");
			if (content.length != 3) {
				this.SendErrorMessage(400, "Client query error!");
			}
			System.out.println("code:" + content[0] + ",filename" + content[1]
					+ ",http version:" + content[2]);
			filename = content[1];

		} catch (IOException e) {
			e.printStackTrace();
		}

		return filename;
	}

	public void SendErrorMessage(int errorCode, String errorMessage) {
		out.println("HTTP/1.0" + errorCode + " " + errorMessage);
		out.println("content-type:text/html");
		out.println();
		out.println("<html><title>Error Message Page</title><head><body><h2>ErrorCode:"
				+ errorCode
				+ "</br>"
				+ errorMessage
				+ "</h2></body></head?</html>");
		out.flush();
		out.close();
		try {
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendFile(String fileName) {
		File file = new File(Processor.WEB_ROOT + fileName);
		if (!file.exists()) {
			SendErrorMessage(404, "File Not found.");
			return;
		}
		try {
			InputStream in = new FileInputStream(file);
			byte content[] = new byte[(int) file.length()];
			in.read(content);
			out.println("HTTP/1.0 200 send query file");
			out.println("content-length:" + content.length);
			out.println();
			out.write(content);
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
